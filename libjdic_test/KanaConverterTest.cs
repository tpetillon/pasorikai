﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Jdic;

namespace JdicTest
{
    [TestClass]
    public class KanaConverterTest
    {
        [TestMethod]
        public void Text_in_hiragana_stays_in_hiragana()
        {
            // Setup
            var input = "にほんご";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("にほんご", output);
        }

        [TestMethod]
        public void Text_in_katakana_is_converted_to_hiragana()
        {
            // Setup
            var input = "テスト";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("てすと", output);
        }

        [TestMethod]
        public void Mixed_hiragana_and_katakana_is_converted_to_hiragana()
        {
            // Setup
            var input = "にほんごテスト";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("にほんごてすと", output);
        }

        [TestMethod]
        public void Half_width_katakana_is_converted_to_fullwidth_hiragana()
        {
            // Setup
            var input = "ﾊﾝｶｸ";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("はんかく", output);
        }

        [TestMethod]
        public void Half_width_voiced_katakana_is_converted_to_fullwidth_hiragana()
        {
            // Setup
            var input = "ﾀﾞｽﾞﾋﾞｺﾞ";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("だずびご", output);
        }

        [TestMethod]
        public void Half_width_semi_voiced_katakana_is_converted_to_fullwidth_hiragana()
        {
            // Setup
            var input = "ﾊﾟﾋﾟﾌﾟﾍﾟﾎﾟ";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("ぱぴぷぺぽ", output);
        }

        [TestMethod]
        public void Kanji_are_not_converted()
        {
            // Setup
            var input = "かなカナ漢字";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("かなかな漢字", output);
        }

        [TestMethod]
        public void Tildes_are_skipped()
        {
            // Setup
            var input = "つめた～い";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("つめたい", output);
        }

        [TestMethod]
        public void Conversion_stops_when_encountering_roman_letters()
        {
            // Setup
            var input = "かなRoman";
            // Execute
            var output = KanaConverter.ToHiragana(input);
            // Verify
            Assert.AreEqual("かな", output);
        }

        [TestMethod]
        public void Length_mapping_for_kana_only_string()
        {
            // Setup
            var input = "にほんご";
            // Execute
            var output = KanaConverter.ToHiraganaWithLengthMapping(input);
            // Verify
            Assert.AreEqual("にほんご", output.Text);
            CollectionAssert.AreEqual(new int[] { 0, 1, 2, 3, 4 }, output.LengthMapping);
        }

        [TestMethod]
        public void Length_mapping_for_mixed_string()
        {
            // Setup
            var input = "かなカナ漢字";
            // Execute
            var output = KanaConverter.ToHiraganaWithLengthMapping(input);
            // Verify
            Assert.AreEqual("かなかな漢字", output.Text);
            CollectionAssert.AreEqual(new int[] { 0, 1, 2, 3, 4, 5, 6 }, output.LengthMapping);
        }

        [TestMethod]
        public void Length_mapping_for_halfwidth_characters()
        {
            // Setup
            var input = "ｱｶﾀﾞﾊﾟﾝ";
            // Execute
            var output = KanaConverter.ToHiraganaWithLengthMapping(input);
            // Verify
            Assert.AreEqual("あかだぱん", output.Text);
            CollectionAssert.AreEqual(new int[] { 0, 1, 2, 4, 6, 7 }, output.LengthMapping);
        }

        [TestMethod]
        public void Length_mapping_for_tilde()
        {
            // Setup
            var input = "つめた～い";
            // Execute
            var output = KanaConverter.ToHiraganaWithLengthMapping(input);
            // Verify
            Assert.AreEqual("つめたい", output.Text);
            CollectionAssert.AreEqual(new int[] { 0, 1, 2, 3, 5 }, output.LengthMapping);
        }
    }
}
