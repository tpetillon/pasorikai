﻿using Rikai;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    public partial class EntryListControl : UserControl
    {
        private List<EntryControl> _entryControls;

        public EntryListControl()
        {
            InitializeComponent();

            _entryControls = new List<EntryControl>();

            var verticalScrollbarWidth = SystemInformation.VerticalScrollBarWidth;
            verticalFlowLayoutPanel.Padding = new Padding(0, 0, verticalScrollbarWidth, 0);

            SizeChanged += EntryListControl_SizeChanged;

            AddMouseEnterCallback(this);
        }

        public void ScrollDown()
        {
            verticalFlowLayoutPanel.AutoScrollPosition = new Point(
                verticalFlowLayoutPanel.AutoScrollPosition.X,
                -verticalFlowLayoutPanel.AutoScrollPosition.Y + Height / 2);
        }

        public void ScrollUp()
        {
            verticalFlowLayoutPanel.AutoScrollPosition = new Point(
                verticalFlowLayoutPanel.AutoScrollPosition.X,
                -verticalFlowLayoutPanel.AutoScrollPosition.Y - Height / 2);
        }

        public void SetEntries(IEnumerable<WordRetriever.SearchResult> entries)
        {
            ClearEntries();

            var i = 0;

            foreach (var entry in entries)
            {
                var entryControl = new EntryControl();

                entryControl.Width = Width - verticalFlowLayoutPanel.Padding.Horizontal;

                entryControl.SetTexts(ConvertEntry(entry));
                verticalFlowLayoutPanel.Controls.Add(entryControl);

                _entryControls.Add(entryControl);

                AddMouseEnterCallback(entryControl);

                if (i % 2 == 0)
                {
                    entryControl.BackColor = SystemColors.ControlLight;
                }

                i++;
            }

            if (_entryControls.Count > 0)
            {
                var lastControl = _entryControls[_entryControls.Count - 1];
                var lastEntryControlMargin = lastControl.Margin;
                lastEntryControlMargin.Bottom = 5;
                lastControl.Margin = lastEntryControlMargin;
            }

            verticalFlowLayoutPanel.HorizontalScroll.Enabled = false;
        }

        private EntryTexts ConvertEntry(WordRetriever.SearchResult entry)
        {
            var readingList = string.Join("、", entry.DictionaryEntry.ReadingElements.Select(r => r.KanaSpelling));
            var deinflection = string.IsNullOrEmpty(entry.Deinflection) ? "" : "(< " + entry.Deinflection + ")";

            var spellingList = entry.DictionaryEntry.SpellingElements
                    .Select(s =>
                        new EntryTexts.WordAndInflection(
                            s.Spelling,
                            readingList,
                            deinflection))
                    .ToArray();

            return new EntryTexts(
                spellingList.Length > 0 ?
                    spellingList :
                    (new EntryTexts.WordAndInflection[] { new EntryTexts.WordAndInflection("", readingList, deinflection) }),
                entry.DictionaryEntry.SenseElements
                    .Select(s =>
                        "(" + string.Join(", ", s.PartsOfSpeech) + ") " +
                        string.Join("; ", s.Glosses
                            .Where(g => g.Language == Jdic.Language.English)
                            .Select(g => g.Text)))
                    .ToArray());
        }

        private void ClearEntries()
        {
            verticalFlowLayoutPanel.Controls.Clear();
            _entryControls.Clear();
        }

        private void EntryListControl_SizeChanged(object sender, System.EventArgs e)
        {
            foreach (var entryControl in _entryControls)
            {
                entryControl.Width = Width - verticalFlowLayoutPanel.Padding.Horizontal;
            }
        }

        private void AddMouseEnterCallback(Control control)
        {
            control.MouseEnter += Control_MouseEnter;

            foreach (Control childControl in control.Controls)
            {
                childControl.MouseEnter += Control_MouseEnter;

                AddMouseEnterCallback(childControl);
            }
        }

        private void Control_MouseEnter(object sender, System.EventArgs e)
        {
            verticalFlowLayoutPanel.Focus();
        }
    }
}
