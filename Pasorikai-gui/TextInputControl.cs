﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    public partial class TextInputControl : UserControl
    {
        public delegate void TextHoverHandler(int? hoveredCharacterIndex);
        public event TextHoverHandler TextHover;

        public override string Text
        {
            set
            {
                textInputLabel.Text = value;
            }
        }

        public TextInputControl()
        {
            InitializeComponent();

            textInputLabel.Text = "";

            AddMouseEnterCallback(this);

            textInputLabel.TextHover += OnTextHover;
        }

        public void OnTextChanged(string newText)
        {
            Text = newText;
        }

        public void OnSelectionChanged(int? selectionStart, int selectionLength)
        {
            textInputLabel.SetHightlight(selectionStart, selectionLength);

            if (!selectionStart.HasValue)
            {
                return;
            }

            var charPosition = textInputLabel.GetCharacterPosition(selectionStart.Value);

            if (-AutoScrollPosition.Y > charPosition.Top)
            {
                AutoScrollPosition = new Point(AutoScrollPosition.X, charPosition.Top);
            }
            else if (Height - AutoScrollPosition.Y < charPosition.Bottom)
            {
                AutoScrollPosition = new Point(AutoScrollPosition.X, charPosition.Bottom - Height);
            }
        }

        public void PinSelection()
        {
            textInputLabel.PinSelection();
        }

        private void OnTextHover(int? hoveredCharacterIndex)
        {
            if (TextHover != null)
            {
                TextHover(hoveredCharacterIndex);
            }
        }

        private void AddMouseEnterCallback(Control control)
        {
            control.MouseEnter += Control_MouseEnter;

            foreach (Control childControl in control.Controls)
            {
                childControl.MouseEnter += Control_MouseEnter;
            }
        }

        private void Control_MouseEnter(object sender, System.EventArgs e)
        {
            Focus();
        }

        protected override Point ScrollToControl(Control activeControl)
        {
            // Returning the current location prevents the panel from
            // scrolling to the active control when the panel loses and regains focus
            return DisplayRectangle.Location;
        }
    }
}
