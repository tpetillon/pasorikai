﻿namespace Pasorikai.GUI
{
    partial class EntryControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verticalFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.japaneseWordControl = new Pasorikai.GUI.JapaneseWordControl();
            this.senseLabel = new Pasorikai.GUI.SenseLabel();
            this.verticalFlowLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // verticalFlowLayoutPanel
            // 
            this.verticalFlowLayoutPanel.AutoSize = true;
            this.verticalFlowLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.verticalFlowLayoutPanel.Controls.Add(this.japaneseWordControl);
            this.verticalFlowLayoutPanel.Controls.Add(this.senseLabel);
            this.verticalFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verticalFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.verticalFlowLayoutPanel.Location = new System.Drawing.Point(0, 15);
            this.verticalFlowLayoutPanel.Name = "verticalFlowLayoutPanel";
            this.verticalFlowLayoutPanel.Size = new System.Drawing.Size(167, 7);
            this.verticalFlowLayoutPanel.TabIndex = 0;
            // 
            // japaneseWordControl
            // 
            this.japaneseWordControl.AutoSize = true;
            this.japaneseWordControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.japaneseWordControl.Location = new System.Drawing.Point(0, 0);
            this.japaneseWordControl.Margin = new System.Windows.Forms.Padding(0);
            this.japaneseWordControl.Name = "japaneseWordControl";
            this.japaneseWordControl.Size = new System.Drawing.Size(166, 24);
            this.japaneseWordControl.TabIndex = 1;
            // 
            // senseLabel
            // 
            this.senseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.senseLabel.AutoSize = true;
            this.senseLabel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.senseLabel.BackColor = System.Drawing.SystemColors.Control;
            this.senseLabel.Location = new System.Drawing.Point(166, 0);
            this.senseLabel.Margin = new System.Windows.Forms.Padding(0);
            this.senseLabel.Name = "senseLabel";
            this.senseLabel.Size = new System.Drawing.Size(166, 13);
            this.senseLabel.TabIndex = 2;
            // 
            // EntryControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.verticalFlowLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "EntryControl";
            this.Padding = new System.Windows.Forms.Padding(0, 15, 0, 15);
            this.Size = new System.Drawing.Size(167, 37);
            this.verticalFlowLayoutPanel.ResumeLayout(false);
            this.verticalFlowLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel verticalFlowLayoutPanel;
        private JapaneseWordControl japaneseWordControl;
        private SenseLabel senseLabel;
    }
}
