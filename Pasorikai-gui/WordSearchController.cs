﻿using Jdic;
using Rikai;
using System;
using System.Globalization;

namespace Pasorikai.GUI
{
    public class WordSearchController
    {
        private readonly TextInputModel _textInputModel;
        private readonly RetrievedEntriesModel _retrievedEntriesModel;
        private readonly WordRetriever _wordRetriever;
        private readonly KanjiDetailsModel _kanjiDetailsModel;
        private readonly KanjiDictionary _kanjiDictionary;

        public WordSearchController(TextInputModel textInputModel, RetrievedEntriesModel retrievedEntriesModel,
            WordRetriever wordRetriever, KanjiDetailsModel kanjiDetailsModel, KanjiDictionary kanjiDictionary)
        {
            _textInputModel = textInputModel;
            _retrievedEntriesModel = retrievedEntriesModel;
            _wordRetriever = wordRetriever;
            _kanjiDetailsModel = kanjiDetailsModel;
            _kanjiDictionary = kanjiDictionary;

            _textInputModel.TextSelectionStartChanged += (int? selectionStart) => RetrieveWords();
            _textInputModel.TextSelectionStartChanged += (int? selectionStart) => RetrieveKanji();
        }

        public void SetText(string text)
        {
            _textInputModel.SetText(text);
        }

        public void SetHighlightStart(int? characterIndex)
        {
            if (characterIndex.HasValue)
            {
                _textInputModel.SetSelectionStart(characterIndex.Value);
            }
            else
            {
                _textInputModel.ClearSelection();
            }
        }

        public void MoveSelectionBackward()
        {
            if (!_textInputModel.SelectionStart.HasValue)
            {
                MoveSelectionToTextBeginning();
            }
            else
            {
                do
                {
                    _textInputModel.SetSelectionStart(_textInputModel.SelectionStart.Value - 1);
                } while (_textInputModel.SelectionStart.Value > 0 &&
                    char.IsWhiteSpace(_textInputModel.CharacterAtSelection));
            }
        }

        public void MoveSelectionForward()
        {
            if (!_textInputModel.SelectionStart.HasValue)
            {
                MoveSelectionToTextBeginning();
            }
            else
            {
                do
                {
                    _textInputModel.SetSelectionStart(_textInputModel.SelectionStart.Value + 1);
                } while (_textInputModel.SelectionStart.Value < _textInputModel.Text.Length - 1 &&
                    char.IsWhiteSpace(_textInputModel.CharacterAtSelection));
            }
        }

        public void MoveSelectionOneWordForward()
        {
            if (!_textInputModel.SelectionStart.HasValue)
            {
                MoveSelectionToTextBeginning();
            }
            else
            {
                do
                {
                    _textInputModel.SetSelectionStart(_textInputModel.SelectionStart.Value + Math.Max(1, _textInputModel.SelectionLength));
                } while (_textInputModel.SelectionStart.Value < _textInputModel.Text.Length - 1 &&
                    char.IsWhiteSpace(_textInputModel.CharacterAtSelection));
            }
        }

        public void MoveSelectionToTextBeginning()
        {
            _textInputModel.SetSelectionStart(0);
        }

        public void MoveSelectionToTextEnd()
        {
            _textInputModel.SetSelectionStart(_textInputModel.Text.Length - 1);
        }

        private async void RetrieveWords()
        {
            var words = await _wordRetriever.GetWordsAsync(_textInputModel.TextFromSelection);

            _retrievedEntriesModel.SetEntries(words);

            if (words.Count == 0)
            {
                _textInputModel.SetSelectionLength(0);
                return;
            }

            _textInputModel.SetSelectionLength(words[0].MatchLength);
        }

        private void RetrieveKanji()
        {
            var graphemeEnumerator = StringInfo.GetTextElementEnumerator(_textInputModel.TextFromSelection);
            if (graphemeEnumerator.MoveNext())
            {
                _kanjiDetailsModel.Kanji = _kanjiDictionary.GetEntry(graphemeEnumerator.GetTextElement());
            }
            else
            {
                _kanjiDetailsModel.Kanji = null;
            }
        }
    }
}
