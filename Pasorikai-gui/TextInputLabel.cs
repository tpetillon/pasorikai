﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    class TextInputLabel : Control
    {
        private Rectangle[] _charPositions;
        private int? _highlightStart;
        private int _highlightLength;
        private bool _pinned;
        
        public delegate void TextHoverHandler(int? selectionStart);
        public event TextHoverHandler TextHover; 

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                _charPositions = new Rectangle[value.Length];
                _highlightStart = null;
                _highlightLength = 0;
                Invalidate(); // Is that really necessary?
            }
        }

        public TextInputLabel()
        {
            DoubleBuffered = true;
            ResizeRedraw = true;

            MouseMove += TextInputLabel_MouseMove;
            MouseEnter += TextInputLabel_MouseEnter;
            MouseLeave += TextInputLabel_MouseLeave;
            MouseClick += TextInputLabel_MouseClick;

            _charPositions = new Rectangle[0];

            _highlightStart = null;
            _highlightLength = 0;
            _pinned = false;
        }

        public void SetHightlight(int? highlightStart, int highlightLength)
        {
            if (highlightStart != _highlightStart || highlightLength != _highlightLength)
            {
                _highlightStart = highlightStart;
                _highlightLength = highlightLength;

                Invalidate();

                _pinned = false;
            }
        }

        public void PinSelection()
        {
            _pinned = true;
        }

        public Rectangle GetCharacterPosition(int characterIndex)
        {
            if (characterIndex >= 0 && characterIndex < _charPositions.Length)
            {
                return _charPositions[characterIndex];
            }

            return default(Rectangle);
        }

        private void TextInputLabel_MouseMove(object sender, MouseEventArgs e)
        {
            if (_pinned)
            {
                return;
            }

            var mousePosition = PointToClient(Control.MousePosition);

            int? highlightStart = null;

            for (var i = 0 ; i < _charPositions.Length ; i++)
            {
                if (_charPositions[i].Contains(mousePosition))
                {
                    highlightStart = i;
                    break;
                }
            }

            if (highlightStart != _highlightStart && TextHover != null)
            {
                TextHover(highlightStart);
            }
        }

        private void TextInputLabel_MouseEnter(object sender, EventArgs e)
        {
            _pinned = false;
        }

        private void TextInputLabel_MouseLeave(object sender, System.EventArgs e)
        {
            if (_pinned)
            {
                return;
            }

            if (_highlightStart.HasValue && TextHover != null)
            {
                TextHover(null);
            }
        }

        private void TextInputLabel_MouseClick(object sender, MouseEventArgs e)
        {
            _pinned = !_pinned;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            var x = Padding.Left;
            var y = Padding.Top;
            var line = "";
            var lineX = x;
            var lineIsHighlighted = false;
            var highlightLength = Math.Max(1, _highlightLength);
            var virtualHighlight = _highlightLength == 0;
            var i = 0;
            var graphemeEnumerator = StringInfo.GetTextElementEnumerator(Text);
            while (graphemeEnumerator.MoveNext())
            {
                var c = graphemeEnumerator.GetTextElement();
                var charSize = TextRenderer.MeasureText(pe.Graphics, c, Font, new Size(100, 100), TextFormatFlags.NoPadding);

                var reachedEndOfLine = Width - Padding.Left - Padding.Right - x < charSize.Width;
                var onLineBreak = c == "\n";
                var onHighlightStart = _highlightStart.HasValue && i == _highlightStart;
                var onHighlightEnd = _highlightStart.HasValue && i == _highlightStart + highlightLength;
                var inHighlight = _highlightStart.HasValue && i >= _highlightStart && i < _highlightStart + highlightLength;

                if (reachedEndOfLine || onLineBreak || onHighlightStart || onHighlightEnd)
                {
                    DrawTextLine(pe.Graphics, line, lineX, y, x - lineX, lineIsHighlighted, virtualHighlight);

                    if (reachedEndOfLine || onLineBreak)
                    {
                        x = Padding.Left;
                        y += Font.Height;
                    }

                    line = "";
                    lineX = x;
                    lineIsHighlighted = inHighlight;
                }

                if (!onLineBreak)
                {
                    line += c;
                }

                _charPositions[i] = new Rectangle(x, y, charSize.Width, charSize.Height);

                x += charSize.Width;

                i++;
            }

            DrawTextLine(pe.Graphics, line, lineX, y, x - lineX, lineIsHighlighted, virtualHighlight);

            Height = y + Font.Height + Padding.Bottom;
        }

        private void DrawTextLine(Graphics graphics, string line, int x, int y, int lineWidth, bool lineIsHighlighted, bool virtualHighlight)
        {
            if (lineIsHighlighted)
            {
                var highlightColor = virtualHighlight ? Brushes.LightGray : Brushes.LightBlue;
                graphics.FillPath(highlightColor, RoundedRectangle.Draw(x, y, lineWidth, Font.Height, Font.Height / 4));
            }

            var color = lineIsHighlighted ? Color.DarkRed: Color.Black;
            TextRenderer.DrawText(graphics, line, Font, new Point(x, y), color, TextFormatFlags.NoPadding);
        }
    }
}
