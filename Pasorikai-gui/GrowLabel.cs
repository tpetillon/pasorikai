﻿using System;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    public class GrowLabel : Label
    {
        private bool _growing;

        public GrowLabel()
        {
            this.AutoSize = false;
        }

        public void ResizeLabel()
        {
            if (_growing)
            {
                return;
            }

            try
            {
                _growing = true;
                var size = new Size(this.Width, Int32.MaxValue);
                size = TextRenderer.MeasureText(this.Text, this.Font, size, TextFormatFlags.WordBreak);
                this.Height = size.Height + Padding.Vertical;
            }
            finally
            {
                _growing = false;
            }
        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
            ResizeLabel();
        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);
            ResizeLabel();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            ResizeLabel();
        }
    }
}
