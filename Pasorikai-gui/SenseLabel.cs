﻿using System.Drawing;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    public partial class SenseLabel : UserControl
    {
        private bool _resizing;

        public override string Text { get { return label.Text; } set { label.Text = value; } }

        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
                label.BackColor = value;
            }
        }

        public SenseLabel()
        {
            InitializeComponent();

            label.SizeChanged += label_SizeChanged;

            _resizing = false;
        }

        void label_SizeChanged(object sender, System.EventArgs e)
        {
            if (_resizing)
            {
                return;
            }

            _resizing = true;

            var size = Size;
            size.Width = label.Width;
            size.Height = label.Height;
            Size = size;

            _resizing = false;
        }
    }
}
