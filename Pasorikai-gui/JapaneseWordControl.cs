﻿using System.Windows.Forms;

namespace Pasorikai.GUI
{
    public partial class JapaneseWordControl : UserControl
    {
        public JapaneseWordControl()
        {
            InitializeComponent();
        }

        public void SetTexts(string kanjiSpelling, string kanaReading, string inflection)
        {
            if (string.IsNullOrWhiteSpace(kanjiSpelling))
            {
                kanjiLabel.Visible = false;
            }
            else
            {
                kanjiLabel.Visible = true;
                kanjiLabel.Text = kanjiSpelling;
            }

            if (string.IsNullOrWhiteSpace(kanaReading))
            {
                kanaLabel.Visible = false;
            }
            else
            {
                kanaLabel.Visible = true;
                kanaLabel.Text = kanaReading;
            }

            if (string.IsNullOrWhiteSpace(inflection))
            {
                inflectionLabel.Visible = false;
            }
            else
            {
                inflectionLabel.Visible = true;
                inflectionLabel.Text = inflection;
            }
        }
    }
}
