﻿using Jdic;
using Rikai;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            topSplitContainer.Paint += SplitContainer_Paint;
            bottomSplitContainer.Paint += SplitContainer_Paint;
            topSplitContainer.Visible = false;
            RecenterLoadingControl();
            Resize += MainForm_Resize;

            Init();
        }

        private async void Init()
        {
            var wordRetreiverTask = BuildWordRetrieverAsync("JMdict", "entities.dat", "freq.dat", "deinflect.dat");
            var kanjiDictionaryTask = BuildKanjiDictionaryAsync("kanjidic2.xml");

            await Task.WhenAll(wordRetreiverTask, kanjiDictionaryTask);

            var wordRetriever = wordRetreiverTask.Result;
            var kanjiDictionary = kanjiDictionaryTask.Result;

            var textInputModel = new TextInputModel();
            var retrievedEntriesModel = new RetrievedEntriesModel();

            var kanjiDetailsModel = new KanjiDetailsModel();

            var wordSearchController = new WordSearchController(
                textInputModel, retrievedEntriesModel, wordRetriever, kanjiDetailsModel, kanjiDictionary);

            textInputModel.TextChanged += textInputControl.OnTextChanged;
            textInputModel.TextSelectionChanged += textInputControl.OnSelectionChanged;

            retrievedEntriesModel.EntriesUpdated += entryListControl.SetEntries;

            kanjiDetailsModel.KanjiChanged += kanjiDetailsControl.SetCharacterElement;

            textInputControl.TextHover += wordSearchController.SetHighlightStart;

            clipboardMonitor.ClipboardChanged += (object sender, ClipboardChangedEventArgs e) =>
            {
                var text = (string) e.DataObject.GetData(typeof(string));
                if (text != null)
                {
                    wordSearchController.SetText(text);
                    wordSearchController.MoveSelectionToTextBeginning();
                }
            };

            keyboardMonitor.Visible = false;

            keyboardMonitor.BackKeyPressed += wordSearchController.MoveSelectionBackward;
            keyboardMonitor.AdvanceKeyPressed += wordSearchController.MoveSelectionForward;
            keyboardMonitor.WordAdvanceKeyPressed += wordSearchController.MoveSelectionOneWordForward;
            keyboardMonitor.GoToBeginningKeyPressed += wordSearchController.MoveSelectionToTextBeginning;
            keyboardMonitor.GoToEndKeyPressed += wordSearchController.MoveSelectionToTextEnd;
            keyboardMonitor.BackKeyPressed += textInputControl.PinSelection;
            keyboardMonitor.AdvanceKeyPressed += textInputControl.PinSelection;
            keyboardMonitor.WordAdvanceKeyPressed += textInputControl.PinSelection;
            keyboardMonitor.GoToBeginningKeyPressed += textInputControl.PinSelection;
            keyboardMonitor.GoToEndKeyPressed += textInputControl.PinSelection;
            keyboardMonitor.ScrollEntriesUp += entryListControl.ScrollUp;
            keyboardMonitor.ScrollEntriesDown += entryListControl.ScrollDown;

            wordSearchController.SetText("");

            loadingControl.Enabled = false;
            loadingControl.Visible = false;
            topSplitContainer.Visible = true;
            topSplitContainer.Invalidate();

            TopMost = true; // For some reason setting this earlier gives a less reliable result.
        }

        private async Task<WordRetriever> BuildWordRetrieverAsync(string dictionaryFilePath,
            string entityFilePath, string frequencyFilePath, string deinflectionRuleFilePath)
        {
            var dictTask = Task.Run(() =>
            {
                var dict = new JdicDictionary();
                var entities = new EntityCollection();
                using (var entityFileReader = new StreamReader(entityFilePath))
                {
                    var entityLoader = new EntityLoader();
                    entityLoader.LoadEntities(entityFileReader, entities);
                }

                using (var dictFileReader = new StreamReader(dictionaryFilePath))
                {
                    var dictLoader = new XmlDictionaryLoader();
                    var entries = dictLoader.LoadDictionary(dictFileReader, entities);
                    dict.AddEntries(entries);
                }

                return Tuple.Create(dict, entities);
            });

            var frequenciesTask = Task.Run(() =>
            {
                Dictionary<string, int> frequencies;
                using (var frequencyFileReader = new StreamReader(frequencyFilePath))
                {
                    var frequencyLoader = new FrequencyLoader();
                    frequencies = frequencyLoader.LoadFrequencies(frequencyFileReader);
                }

                return frequencies;
            });

            var deinflectorTask = Task.Run(() =>
            {
                return BuildDeinflector(deinflectionRuleFilePath);
            });

            await Task.WhenAll(dictTask, frequenciesTask, deinflectorTask);

            return new WordRetriever(dictTask.Result.Item1, frequenciesTask.Result, deinflectorTask.Result);
        }

        private Deinflector BuildDeinflector(string ruleFilePath)
        {
            using (var ruleFileReader = new StreamReader(ruleFilePath))
            {
                var ruleLoader = new DeinflectionRuleLoader();
                var reasonsAndRules = ruleLoader.LoadRules(ruleFileReader);

                return new Deinflector(reasonsAndRules.Item1, reasonsAndRules.Item2);
            }
        }

        private Task<KanjiDictionary> BuildKanjiDictionaryAsync(string dictionaryFilePath)
        {
            return Task.Run(() => BuildKanjiDictionary(dictionaryFilePath));
        }

        private KanjiDictionary BuildKanjiDictionary(string dictionaryFilePath)
        {
            var dict = new KanjiDictionary();

            using (var dictFileReader = new StreamReader(dictionaryFilePath))
            {
                var dictLoader = new XmlKanjiLoader();
                var entries = dictLoader.LoadCharacters(dictFileReader);
                dict.AddEntries(entries);
            }

            return dict;
        }

        private void SplitContainer_Paint(object sender, PaintEventArgs e)
        {
            var splitContainer = sender as SplitContainer;

            if (splitContainer != null)
            {
                var dotSize = 4;
                var spacing = 10;
                var splitterRectangle = splitContainer.SplitterRectangle;

                e.Graphics.FillEllipse(Brushes.Silver, new Rectangle(
                    splitterRectangle.X + splitterRectangle.Width / 2 - dotSize / 2 - spacing,
                    splitterRectangle.Y + splitterRectangle.Height / 2 - dotSize / 2,
                    dotSize, dotSize));
                e.Graphics.FillEllipse(Brushes.Silver, new Rectangle(
                    splitterRectangle.X + splitterRectangle.Width / 2 - dotSize / 2,
                    splitterRectangle.Y + splitterRectangle.Height / 2 - dotSize / 2,
                    dotSize, dotSize));
                e.Graphics.FillEllipse(Brushes.Silver, new Rectangle(
                    splitterRectangle.X + splitterRectangle.Width / 2 - dotSize / 2 + spacing,
                    splitterRectangle.Y + splitterRectangle.Height / 2 - dotSize / 2,
                    dotSize, dotSize));
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            RecenterLoadingControl();

            topSplitContainer.Invalidate();
            bottomSplitContainer.Invalidate();
        }

        private void RecenterLoadingControl()
        {
            loadingControl.Left = (ClientSize.Width - loadingControl.Width) / 2;
            loadingControl.Top = Math.Max((ClientSize.Height - loadingControl.Height * 8) / 2, 20);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            keyboardMonitor.RegisterHotKeys();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            keyboardMonitor.UnregisterHotKeys();
        }
    }
}
