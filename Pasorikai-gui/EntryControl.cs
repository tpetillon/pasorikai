﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    public partial class EntryControl : UserControl
    {
        private List<SenseLabel> _senseLabels = new List<SenseLabel>();
        private bool _resizing = false;

        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;

                verticalFlowLayoutPanel.BackColor = value;

                japaneseWordControl.BackColor = value;

                foreach (var senseLabel in _senseLabels)
                {
                    senseLabel.BackColor = value;
                }
            }
        }

        public EntryControl()
        {
            InitializeComponent();

            SizeChanged += EntryControl_SizeChanged;
        }

        public void SetTexts(EntryTexts texts)
        {
            DeleteDefaultTexts();

            var height = Padding.Top;

            foreach (var word in texts.Words)
            {
                var wordControl = new JapaneseWordControl();
                wordControl.SetTexts(word.Word, word.Reading, word.Inflection);
                verticalFlowLayoutPanel.Controls.Add(wordControl);

                height += wordControl.Height;
            }

            foreach (var sense in texts.Senses)
            {
                var senseLabel = new SenseLabel();

                senseLabel.Width = Width;

                senseLabel.Text = sense;
                verticalFlowLayoutPanel.Controls.Add(senseLabel);

                _senseLabels.Add(senseLabel);

                height += senseLabel.Height;
            }

            Height = height + Padding.Bottom;
        }

        private void DeleteDefaultTexts()
        {
            verticalFlowLayoutPanel.Controls.Remove(japaneseWordControl);
            verticalFlowLayoutPanel.Controls.Remove(senseLabel);
        }

        private void EntryControl_SizeChanged(object sender, EventArgs e)
        {
            if (_resizing)
            {
                return;
            }

            _resizing = true;

            foreach (var senseLabel in _senseLabels)
            {
                senseLabel.Width = Width;
            }

            var height = Padding.Top;

            foreach (Control control in verticalFlowLayoutPanel.Controls)
            {
                height += control.Height;
            }

            Height = height + Padding.Bottom;

            _resizing = false;
        }
    }
}
