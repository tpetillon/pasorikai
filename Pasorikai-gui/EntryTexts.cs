﻿using System;
using System.Collections.Generic;

namespace Pasorikai.GUI
{
    public struct EntryTexts
    {
        public struct WordAndInflection
        {
            public readonly string Word;
            public readonly string Reading;
            public readonly string Inflection;

            public WordAndInflection(string word, string reading, string inflection)
            {
                Word = word;
                Reading = reading;
                Inflection = inflection;
            }
        }

        public readonly IEnumerable<WordAndInflection> Words;
        public readonly IEnumerable<string> Senses;

        public EntryTexts(IEnumerable<WordAndInflection> words, IEnumerable<string> senses)
        {
            Words = words;
            Senses = senses;
        }
    }
}
