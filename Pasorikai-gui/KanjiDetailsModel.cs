﻿using Jdic;

namespace Pasorikai.GUI
{
    public class KanjiDetailsModel
    {
        private CharacterElement _kanji;

        public delegate void KanjiChangedHandler(CharacterElement newKanji);
        public event KanjiChangedHandler KanjiChanged;

        public CharacterElement Kanji
        {
            get
            {
                return _kanji;
            }
            set
            {
                if (_kanji == value)
                {
                    return;
                }

                _kanji = value;

                if (KanjiChanged != null)
                {
                    KanjiChanged(_kanji);
                }
            }
        }

        public KanjiDetailsModel()
        {
            _kanji = null;
        }
    }
}
