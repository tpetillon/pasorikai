﻿namespace Pasorikai.GUI
{
    partial class EntryListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verticalFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // verticalFlowLayoutPanel
            // 
            this.verticalFlowLayoutPanel.AutoScroll = true;
            this.verticalFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verticalFlowLayoutPanel.Location = new System.Drawing.Point(0, 4);
            this.verticalFlowLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.verticalFlowLayoutPanel.Name = "verticalFlowLayoutPanel";
            this.verticalFlowLayoutPanel.Size = new System.Drawing.Size(506, 318);
            this.verticalFlowLayoutPanel.TabIndex = 0;
            // 
            // EntryListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.verticalFlowLayoutPanel);
            this.Name = "EntryListControl";
            this.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.Size = new System.Drawing.Size(506, 322);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel verticalFlowLayoutPanel;

    }
}
