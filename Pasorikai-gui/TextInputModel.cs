﻿namespace Pasorikai.GUI
{
    public class TextInputModel
    {
        private string _text;
        private int? _selectionStart;
        private int _selectionLength;

        public delegate void TextChangedHandler(string newText);
        public event TextChangedHandler TextChanged;

        public delegate void TextSelectionStartChangedHandler(int? selectionStart);
        public event TextSelectionStartChangedHandler TextSelectionStartChanged;

        public delegate void TextSelectionChangedHandler(int? selectionStart, int selectionLength);
        public event TextSelectionChangedHandler TextSelectionChanged;

        public string Text { get { return _text; } }
        
        public string TextFromSelection
        {
            get
            {
                if (!_selectionStart.HasValue)
                {
                    return "";
                }

                return _text.Substring(_selectionStart.Value);
            }
        }

        public char CharacterAtSelection
        {
            get
            {
                if (!_selectionStart.HasValue)
                {
                    return '\0';
                }

                return _text[_selectionStart.Value];
            }
        }

        public int? SelectionStart { get { return _selectionStart; } }
        public int SelectionLength { get { return _selectionLength; } }

        public TextInputModel()
        {
            _text = "";
            _selectionStart = null;
            _selectionLength = 0;
        }

        public void SetText(string text)
        {
            _text = text.Trim();

            if (TextChanged != null)
            {
                TextChanged(text);
            }

            ClearSelection();
        }

        public void ClearSelection()
        {
            _selectionStart = null;
            _selectionLength = 0;

            if (TextSelectionStartChanged != null)
            {
                TextSelectionStartChanged(_selectionStart);
            }

            if (TextSelectionChanged != null)
            {
                TextSelectionChanged(_selectionStart, _selectionLength);
            }
        }

        public void SetSelectionStart(int selectionStart)
        {
            if (string.IsNullOrEmpty(_text))
            {
                return;
            }

            var newSelectionStart = selectionStart;

            if (newSelectionStart < 0)
            {
                newSelectionStart = 0; 
            }
            else if (newSelectionStart >= _text.Length)
            {
                newSelectionStart = _text.Length - 1;
            }
            
            if (newSelectionStart == _selectionStart)
            {
                return;
            }

            _selectionStart = newSelectionStart;
            _selectionLength = 0;

            if (TextSelectionStartChanged != null)
            {
                TextSelectionStartChanged(_selectionStart);
            }

            if (TextSelectionChanged != null)
            {
                TextSelectionChanged(_selectionStart, _selectionLength);
            }
        }

        public void SetSelectionLength(int selectionLength)
        {
            var newSelectionLength = selectionLength;

            if (newSelectionLength < 0)
            {
                newSelectionLength = 0;
            }
            else if (_selectionStart + newSelectionLength > _text.Length)
            {
                newSelectionLength = 1;
            }

            if (newSelectionLength == _selectionLength)
            {
                return;
            }

            _selectionLength = newSelectionLength;

            if (TextSelectionChanged != null)
            {
                TextSelectionChanged(_selectionStart, _selectionLength);
            }
        }
    }
}
