﻿using Jdic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    /*
    public abstract partial class KanjiReadingMeaningControlBase : UserControl
    {
        private Font _japaneseFont;
        public int _a;

        [Description("Font use to display on and kun readings"), Category("Appearance")]
        public Font JapaneseFont { get { return _japaneseFont; } set { _japaneseFont = value; } }
        [Description("Font use to display on and kun readings"), Category("Appearance")]
        public int Aaaa { get { return _a; } set { _a = value; } }
    }
    */
    public partial class KanjiReadingMeaningControl : UserControl
    {
        private Font _japaneseFont;

        public KanjiReadingMeaningControl()
        {
            InitializeComponent();

            _japaneseFont = new Font("Meiryo", 10);

            flowLayoutPanel.Resize += flowLayoutPanel_Resize;
        }

        public void SetReadingMeaning(ReadingMeaningElement readingMeaning)
        {
            flowLayoutPanel.Controls.Clear();

            var onReadings = readingMeaning.Readings
                .Where(r => r.Type == ReadingMeaningElement.ReadingType.JapaneseOn)
                .Select(r => r.Reading);

            var onReadingsLabel = new GrowLabel();
            flowLayoutPanel.Controls.Add(onReadingsLabel);
            onReadingsLabel.Text = string.Join("、", onReadings);
            onReadingsLabel.Font = _japaneseFont;
            onReadingsLabel.ForeColor = Color.SteelBlue;
            onReadingsLabel.Width = flowLayoutPanel.Width;
            onReadingsLabel.Margin = new Padding(0, 0, 0, 4);

            var kunReadings = readingMeaning.Readings
                .Where(r => r.Type == ReadingMeaningElement.ReadingType.JapaneseKun)
                .Select(r => r.Reading);

            var kunReadingsLabel = new GrowLabel();
            flowLayoutPanel.Controls.Add(kunReadingsLabel);
            kunReadingsLabel.Text = string.Join("、", kunReadings);
            kunReadingsLabel.Font = _japaneseFont;
            kunReadingsLabel.ForeColor = Color.Green;
            kunReadingsLabel.Width = flowLayoutPanel.Width;
            kunReadingsLabel.Margin = new Padding(0, 0, 0, 4);

            var meanings = readingMeaning.Meanings
                .Where(m => m.Language == Language.English)
                .Select(m => m.Meaning);

            var meaningsLabel = new GrowLabel();
            flowLayoutPanel.Controls.Add(meaningsLabel);
            meaningsLabel.Text = string.Join(", ", meanings);
            meaningsLabel.Width = flowLayoutPanel.Width;

            ResizeHeight();
        }

        private void flowLayoutPanel_Resize(object sender, System.EventArgs e)
        {
            foreach (Control control in flowLayoutPanel.Controls)
            {
                control.Width = flowLayoutPanel.Width;
            }

            ResizeHeight();
        }

        private void ResizeHeight()
        {
            var height = 0;
            foreach (Control control in flowLayoutPanel.Controls)
            {
                height += control.Height + control.Margin.Vertical;
            }

            flowLayoutPanel.Height = height;
            Height = height;
        }
    }
}
