﻿namespace Pasorikai.GUI
{
    partial class JapaneseWordControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kanjiLabel = new System.Windows.Forms.Label();
            this.kanaLabel = new System.Windows.Forms.Label();
            this.inflectionLabel = new System.Windows.Forms.Label();
            this.horizontalFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.horizontalFlowLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // kanjiLabel
            // 
            this.kanjiLabel.AutoSize = true;
            this.kanjiLabel.Font = new System.Drawing.Font("Meiryo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kanjiLabel.ForeColor = System.Drawing.Color.SteelBlue;
            this.kanjiLabel.Location = new System.Drawing.Point(3, 0);
            this.kanjiLabel.Name = "kanjiLabel";
            this.kanjiLabel.Size = new System.Drawing.Size(42, 24);
            this.kanjiLabel.TabIndex = 0;
            this.kanjiLabel.Text = "漢字";
            // 
            // kanaLabel
            // 
            this.kanaLabel.AutoSize = true;
            this.kanaLabel.Font = new System.Drawing.Font("Meiryo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kanaLabel.ForeColor = System.Drawing.Color.Green;
            this.kanaLabel.Location = new System.Drawing.Point(51, 0);
            this.kanaLabel.Name = "kanaLabel";
            this.kanaLabel.Size = new System.Drawing.Size(42, 24);
            this.kanaLabel.TabIndex = 1;
            this.kanaLabel.Text = "かな";
            // 
            // inflectionLabel
            // 
            this.inflectionLabel.AutoSize = true;
            this.inflectionLabel.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.inflectionLabel.Location = new System.Drawing.Point(99, 0);
            this.inflectionLabel.Name = "inflectionLabel";
            this.inflectionLabel.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
            this.inflectionLabel.Size = new System.Drawing.Size(64, 20);
            this.inflectionLabel.TabIndex = 2;
            this.inflectionLabel.Text = "(< inflection)";
            // 
            // horizontalFlowLayoutPanel
            // 
            this.horizontalFlowLayoutPanel.AutoSize = true;
            this.horizontalFlowLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.horizontalFlowLayoutPanel.Controls.Add(this.kanjiLabel);
            this.horizontalFlowLayoutPanel.Controls.Add(this.kanaLabel);
            this.horizontalFlowLayoutPanel.Controls.Add(this.inflectionLabel);
            this.horizontalFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.horizontalFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.horizontalFlowLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.horizontalFlowLayoutPanel.Name = "horizontalFlowLayoutPanel";
            this.horizontalFlowLayoutPanel.Size = new System.Drawing.Size(166, 24);
            this.horizontalFlowLayoutPanel.TabIndex = 3;
            // 
            // JapaneseWordControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.horizontalFlowLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "JapaneseWordControl";
            this.Size = new System.Drawing.Size(166, 24);
            this.horizontalFlowLayoutPanel.ResumeLayout(false);
            this.horizontalFlowLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label kanjiLabel;
        private System.Windows.Forms.Label kanaLabel;
        private System.Windows.Forms.Label inflectionLabel;
        private System.Windows.Forms.FlowLayoutPanel horizontalFlowLayoutPanel;
    }
}
