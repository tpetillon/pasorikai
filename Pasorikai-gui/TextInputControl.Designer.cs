﻿namespace Pasorikai.GUI
{
    partial class TextInputControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textInputLabel = new Pasorikai.GUI.TextInputLabel();
            this.SuspendLayout();
            // 
            // textInputLabel
            // 
            this.textInputLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textInputLabel.Font = new System.Drawing.Font("Meiryo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textInputLabel.Location = new System.Drawing.Point(0, 0);
            this.textInputLabel.Name = "textInputLabel";
            this.textInputLabel.Padding = new System.Windows.Forms.Padding(3);
            this.textInputLabel.Size = new System.Drawing.Size(387, 40);
            this.textInputLabel.TabIndex = 0;
            this.textInputLabel.Text = "テキスト入力";
            // 
            // TextInputControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.textInputLabel);
            this.Name = "TextInputControl";
            this.Size = new System.Drawing.Size(387, 220);
            this.ResumeLayout(false);

        }

        #endregion

        private TextInputLabel textInputLabel;
    }
}
