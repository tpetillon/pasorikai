﻿using Rikai;
using System.Collections.Generic;

namespace Pasorikai.GUI
{
    public class RetrievedEntriesModel
    {
        public delegate void EntriesUpdatedHandler(IList<WordRetriever.SearchResult> entries);
        public event EntriesUpdatedHandler EntriesUpdated;

        private readonly List<WordRetriever.SearchResult> _entries;

        public IList<WordRetriever.SearchResult> Entries { get { return _entries.AsReadOnly(); } }

        public RetrievedEntriesModel()
        {
            _entries = new List<WordRetriever.SearchResult>();
        }

        public void SetEntries(IEnumerable<WordRetriever.SearchResult> entries)
        {
            _entries.Clear();
            _entries.AddRange(entries);

            if (EntriesUpdated != null)
            {
                EntriesUpdated(Entries);
            }
        }
    }
}
