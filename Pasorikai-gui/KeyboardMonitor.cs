﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Input;

namespace Pasorikai.GUI
{
    public class KeyboardMonitor : Control
    {
        public delegate void KeyPressedHandler();
        public event KeyPressedHandler BackKeyPressed;
        public event KeyPressedHandler AdvanceKeyPressed;
        public event KeyPressedHandler WordAdvanceKeyPressed;
        public event KeyPressedHandler GoToBeginningKeyPressed;
        public event KeyPressedHandler GoToEndKeyPressed;
        public event KeyPressedHandler ScrollEntriesDown;
        public event KeyPressedHandler ScrollEntriesUp;

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode, IntPtr wParam, IntPtr lParam);

        public const int WH_KEYBOARD_LL = 13;
        public const int WM_KEYDOWN = 0x100;

        [StructLayout(LayoutKind.Sequential)]
        public class KbLLHookStruct
        {
            public int vkCode;
            public int scanCode;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }

        public delegate int HookProc(int nCode, IntPtr wParam, IntPtr lParam);

        private static int hookHandle = 0;
        private static HookProc callbackDelegate;

        private int KbHookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && wParam.ToInt32() == WM_KEYDOWN)
            {
                var hookStruct = (KbLLHookStruct)Marshal.PtrToStructure(lParam, typeof(KbLLHookStruct));

                switch (hookStruct.vkCode)
                {
                    case (int) Keys.NumPad4:
                        if (BackKeyPressed != null)
                        {
                            BackKeyPressed();
                        }
                        break;
                    case (int) Keys.NumPad5:
                        if (AdvanceKeyPressed != null)
                        {
                            AdvanceKeyPressed();
                        }
                        break;
                    case (int) Keys.NumPad6:
                        if (WordAdvanceKeyPressed != null)
                        {
                            WordAdvanceKeyPressed();
                        }
                        break;
                    case (int) Keys.NumPad7:
                        if (GoToBeginningKeyPressed != null)
                        {
                            GoToBeginningKeyPressed();
                        }
                        break;
                    case (int) Keys.NumPad1:
                        if (GoToEndKeyPressed != null)
                        {
                            GoToEndKeyPressed();
                        }
                        break;
                    case (int) Keys.NumPad9:
                        if (ScrollEntriesUp != null)
                        {
                            ScrollEntriesUp();
                        }
                        break;
                    case (int) Keys.NumPad3:
                        if (ScrollEntriesDown != null)
                        {
                            ScrollEntriesDown();
                        }
                        break;
                }
            }
            
            // Pass to other keyboard handlers.
            return CallNextHookEx(hookHandle, nCode, wParam, lParam);
        }

        public void RegisterHotKeys()
        {
            if (callbackDelegate != null)
            {
                return;
            }

            callbackDelegate = new HookProc(KbHookProc);

            // Set system-wide hook.
            hookHandle = SetWindowsHookEx(WH_KEYBOARD_LL, callbackDelegate, (IntPtr)0, 0);

            if (hookHandle == 0)
            {
                // Hook failed
                callbackDelegate = null;
            }
        }

        public void UnregisterHotKeys()
        {
            if (callbackDelegate == null)
            {
                return;
            }

            var ok = UnhookWindowsHookEx(hookHandle);

            if (ok)
            {
                callbackDelegate = null;
            }
        }
    }
}
