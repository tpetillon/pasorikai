﻿using Jdic;
using System.Drawing;
using System.Windows.Forms;

namespace Pasorikai.GUI
{
    public partial class KanjiDetailsControl : UserControl
    {
        public KanjiDetailsControl()
        {
            InitializeComponent();

            kanjiLabel.Text = "";

            flowLayoutPanel.Resize += flowLayoutPanel_Resize;

            AddMouseEnterCallback(this);
        }

        public void SetCharacterElement(CharacterElement element)
        {
            flowLayoutPanel.Controls.Clear();

            if (element == null)
            {
                kanjiLabel.Text = "";    
                return;
            }

            kanjiLabel.Text = element.Literal;

            var i = 0;
            foreach (var readingMeaning in element.ReadingMeanings)
            {
                var control = new KanjiReadingMeaningControl();
                flowLayoutPanel.Controls.Add(control);
                control.Width = flowLayoutPanel.Width;
                control.SetReadingMeaning(readingMeaning);

                AddMouseEnterCallback(control);

                if (i % 2 == 1)
                {
                    control.BackColor = SystemColors.ControlLight;
                }

                i++;
            }

            ResizeHeight();

            AutoScrollPosition = new Point(0, 0);
        }

        private void flowLayoutPanel_Resize(object sender, System.EventArgs e)
        {
            foreach (Control control in flowLayoutPanel.Controls)
            {
                control.Width = flowLayoutPanel.Width;
            }

            ResizeHeight();
        }

        private void ResizeHeight()
        {
            var height = 0;
            foreach (Control control in flowLayoutPanel.Controls)
            {
                height += control.Height;
            }

            flowLayoutPanel.Height = height;
        }

        private void AddMouseEnterCallback(Control control)
        {
            control.MouseEnter += Control_MouseEnter;

            foreach (Control childControl in control.Controls)
            {
                childControl.MouseEnter += Control_MouseEnter;

                AddMouseEnterCallback(childControl);
            }
        }

        private void Control_MouseEnter(object sender, System.EventArgs e)
        {
            Focus();
        }

        protected override Point ScrollToControl(Control activeControl)
        {
            // Returning the current location prevents the panel from
            // scrolling to the active control when the panel loses and regains focus
            return DisplayRectangle.Location;
        }
    }
}
