﻿namespace Pasorikai.GUI
{
    partial class KanjiDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kanjiLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // kanjiLabel
            // 
            this.kanjiLabel.Font = new System.Drawing.Font("Meiryo", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kanjiLabel.Location = new System.Drawing.Point(-6, -9);
            this.kanjiLabel.Name = "kanjiLabel";
            this.kanjiLabel.Size = new System.Drawing.Size(100, 85);
            this.kanjiLabel.TabIndex = 0;
            this.kanjiLabel.Text = "漢";
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel.Location = new System.Drawing.Point(78, 0);
            this.flowLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(351, 113);
            this.flowLayoutPanel.TabIndex = 1;
            // 
            // KanjiDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.kanjiLabel);
            this.MinimumSize = new System.Drawing.Size(0, 80);
            this.Name = "KanjiDetailsControl";
            this.Size = new System.Drawing.Size(429, 113);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label kanjiLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
    }
}
