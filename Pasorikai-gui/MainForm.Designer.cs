﻿namespace Pasorikai.GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.topSplitContainer = new System.Windows.Forms.SplitContainer();
            this.bottomSplitContainer = new System.Windows.Forms.SplitContainer();
            this.loadingControl = new Pasorikai.GUI.LoadingControl();
            this.keyboardMonitor = new Pasorikai.GUI.KeyboardMonitor();
            this.clipboardMonitor = new Pasorikai.GUI.ClipboardMonitor();
            this.textInputControl = new Pasorikai.GUI.TextInputControl();
            this.entryListControl = new Pasorikai.GUI.EntryListControl();
            this.kanjiDetailsControl = new Pasorikai.GUI.KanjiDetailsControl();
            ((System.ComponentModel.ISupportInitialize)(this.topSplitContainer)).BeginInit();
            this.topSplitContainer.Panel1.SuspendLayout();
            this.topSplitContainer.Panel2.SuspendLayout();
            this.topSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottomSplitContainer)).BeginInit();
            this.bottomSplitContainer.Panel1.SuspendLayout();
            this.bottomSplitContainer.Panel2.SuspendLayout();
            this.bottomSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // topSplitContainer
            // 
            this.topSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.topSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.topSplitContainer.Name = "topSplitContainer";
            this.topSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // topSplitContainer.Panel1
            // 
            this.topSplitContainer.Panel1.Controls.Add(this.textInputControl);
            this.topSplitContainer.Panel1MinSize = 100;
            // 
            // topSplitContainer.Panel2
            // 
            this.topSplitContainer.Panel2.Controls.Add(this.bottomSplitContainer);
            this.topSplitContainer.Panel2MinSize = 100;
            this.topSplitContainer.Size = new System.Drawing.Size(512, 719);
            this.topSplitContainer.SplitterDistance = 164;
            this.topSplitContainer.TabIndex = 3;
            // 
            // bottomSplitContainer
            // 
            this.bottomSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bottomSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.bottomSplitContainer.Name = "bottomSplitContainer";
            this.bottomSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // bottomSplitContainer.Panel1
            // 
            this.bottomSplitContainer.Panel1.Controls.Add(this.entryListControl);
            this.bottomSplitContainer.Panel1MinSize = 60;
            // 
            // bottomSplitContainer.Panel2
            // 
            this.bottomSplitContainer.Panel2.Controls.Add(this.kanjiDetailsControl);
            this.bottomSplitContainer.Panel2MinSize = 80;
            this.bottomSplitContainer.Size = new System.Drawing.Size(512, 551);
            this.bottomSplitContainer.SplitterDistance = 464;
            this.bottomSplitContainer.TabIndex = 5;
            // 
            // loadingControl
            // 
            this.loadingControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loadingControl.AutoSize = true;
            this.loadingControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.loadingControl.Location = new System.Drawing.Point(205, 60);
            this.loadingControl.Name = "loadingControl";
            this.loadingControl.Size = new System.Drawing.Size(120, 34);
            this.loadingControl.TabIndex = 1;
            // 
            // keyboardMonitor
            // 
            this.keyboardMonitor.BackColor = System.Drawing.Color.Lime;
            this.keyboardMonitor.Location = new System.Drawing.Point(78, 0);
            this.keyboardMonitor.Name = "keyboardMonitor";
            this.keyboardMonitor.Size = new System.Drawing.Size(75, 23);
            this.keyboardMonitor.TabIndex = 5;
            this.keyboardMonitor.Text = "keyboardMonitor";
            this.keyboardMonitor.Visible = false;
            // 
            // clipboardMonitor
            // 
            this.clipboardMonitor.BackColor = System.Drawing.Color.Red;
            this.clipboardMonitor.Location = new System.Drawing.Point(0, 0);
            this.clipboardMonitor.Name = "clipboardMonitor";
            this.clipboardMonitor.Size = new System.Drawing.Size(75, 23);
            this.clipboardMonitor.TabIndex = 4;
            this.clipboardMonitor.Text = "clipboardMonitor";
            this.clipboardMonitor.Visible = false;
            // 
            // textInputControl
            // 
            this.textInputControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textInputControl.AutoScroll = true;
            this.textInputControl.Location = new System.Drawing.Point(0, 0);
            this.textInputControl.Name = "textInputControl";
            this.textInputControl.Size = new System.Drawing.Size(512, 162);
            this.textInputControl.TabIndex = 0;
            // 
            // entryListControl
            // 
            this.entryListControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.entryListControl.Location = new System.Drawing.Point(0, 0);
            this.entryListControl.Name = "entryListControl";
            this.entryListControl.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.entryListControl.Size = new System.Drawing.Size(512, 461);
            this.entryListControl.TabIndex = 4;
            // 
            // kanjiDetailsControl
            // 
            this.kanjiDetailsControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kanjiDetailsControl.AutoScroll = true;
            this.kanjiDetailsControl.Location = new System.Drawing.Point(0, 0);
            this.kanjiDetailsControl.MinimumSize = new System.Drawing.Size(0, 80);
            this.kanjiDetailsControl.Name = "kanjiDetailsControl";
            this.kanjiDetailsControl.Size = new System.Drawing.Size(512, 80);
            this.kanjiDetailsControl.TabIndex = 6;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 721);
            this.Controls.Add(this.loadingControl);
            this.Controls.Add(this.keyboardMonitor);
            this.Controls.Add(this.clipboardMonitor);
            this.Controls.Add(this.topSplitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(60, 160);
            this.Name = "MainForm";
            this.Text = "Pasorikai";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.topSplitContainer.Panel1.ResumeLayout(false);
            this.topSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.topSplitContainer)).EndInit();
            this.topSplitContainer.ResumeLayout(false);
            this.bottomSplitContainer.Panel1.ResumeLayout(false);
            this.bottomSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bottomSplitContainer)).EndInit();
            this.bottomSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer topSplitContainer;
        private EntryListControl entryListControl;
        private TextInputControl textInputControl;
        private ClipboardMonitor clipboardMonitor;
        private KeyboardMonitor keyboardMonitor;
        private LoadingControl loadingControl;
        private KanjiDetailsControl kanjiDetailsControl;
        private System.Windows.Forms.SplitContainer bottomSplitContainer;

    }
}

