﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace Pasorikai.GUI
{
    static class RoundedRectangle
    {
        public static GraphicsPath Draw(int x, int y, int width, int height, int cornerRadius)
        {
            var xL = x + cornerRadius;
            var xR = x + width - cornerRadius;
            var yT = y + cornerRadius;
            var yB = y + height - cornerRadius;

            var diameter = cornerRadius * 2;

            var path = new GraphicsPath();
            path.StartFigure();
            path.AddLine(xL, y, xR, y);
            path.AddArc(xR - cornerRadius, y, diameter, diameter, -90, 90);
            path.AddLine(x + width, yT, x + width, yB);
            path.AddArc(xR - cornerRadius, yB - cornerRadius, diameter, diameter, 0, 90);
            path.AddLine(xR, y + height, xL, y + height);
            path.AddArc(x, yB - cornerRadius, diameter, diameter, 90, 90);
            path.AddLine(x, yB, x, yT);
            path.AddArc(x, y, diameter, diameter, 180, 90);

            path.CloseFigure();

            return path;
        }
    }
}
