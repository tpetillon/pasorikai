﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Rikai
{
    public class FrequencyLoader
    {
        public Dictionary<string, int> LoadFrequencies(TextReader input)
        {
            var frequencies = new Dictionary<string, int>();

            var splitChars = new char[] { ',' };

            string line;
            while (!string.IsNullOrEmpty(line = input.ReadLine()))
            {
                var splits = line.Split(splitChars);

                var word = splits[0];
                var frequency = int.Parse(splits[1]);

                frequencies.Add(word, frequency);
            }

            return frequencies;
        }
    }
}
