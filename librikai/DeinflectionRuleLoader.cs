﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Rikai
{
    public class DeinflectionRuleLoader
    {
        public Tuple<List<string>, List<Deinflector.RuleGroup>> LoadRules(TextReader input)
        {
            var reasons = new List<string>();
            var ruleList = new List<Deinflector.Rule>();
            var fromLength = -1;
            var ruleGroups = new List<Deinflector.RuleGroup>();

            var ruleCount = 0;

            // skip header
            input.ReadLine();

            var splitChars = new char[] { '\t' };

            string line;
            while ((line = input.ReadLine()) != null)
            {
                var splits = line.Split(splitChars);

                if (splits.Length == 1)
                {
                    // reason line
                    reasons.Add(splits[0]);
                }
                else if (splits.Length == 4)
                {
                    // rule line
                    var from = splits[0];
                    var to = splits[1];
                    var type = int.Parse(splits[2]);
                    var reason = int.Parse(splits[3]);

                    if (from.Length != fromLength)
                    {
                        if (ruleList.Count > 0)
                        {
                            ruleGroups.Add(new Deinflector.RuleGroup(ruleList.ToArray(), fromLength));
                            ruleList = new List<Deinflector.Rule>();
                        }

                        fromLength = from.Length;
                    }

                    ruleList.Add(new Deinflector.Rule(from, to, type, reason));

                    ruleCount++;
                }
            }

            ruleGroups.Add(new Deinflector.RuleGroup(ruleList.ToArray(), fromLength));

            return Tuple.Create(reasons, ruleGroups);
        }
    }
}
