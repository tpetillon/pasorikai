﻿using Jdic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rikai
{
    public class WordRetriever
    {
        public struct SearchResult
        {
            public readonly JdicEntry DictionaryEntry;
            public readonly string Deinflection;
            public readonly int MatchLength;

            public SearchResult(JdicEntry dictionaryEntry, string deinflection, int matchLength)
            {
                DictionaryEntry = dictionaryEntry;
                Deinflection = deinflection;
                MatchLength = matchLength;
            }
        }

        private readonly JdicDictionary _dict;
        private readonly Dictionary<string, int> _frequencies;
        private readonly Deinflector _deinflector;

        public WordRetriever(JdicDictionary dict, Dictionary<string, int> frequencies, Deinflector deinflector)
        {
            _dict = dict;
            _frequencies = frequencies;
            _deinflector = deinflector;
        }

        public List<SearchResult> GetWords(string searchString)
        {
            var results = new List<SearchResult>();

            var conversionResult = KanaConverter.ToHiraganaWithLengthMapping(searchString);

            if (string.IsNullOrWhiteSpace(conversionResult.Text))
            {
                return results;
            }

            searchString = conversionResult.Text;
            searchString = searchString.Substring(0, Math.Min(searchString.Length, _dict.MaxEntryLength));

            do
            {
                var matchLength = conversionResult.LengthMapping[searchString.Length];

                var deinflections = _deinflector.Deinflect(searchString);

                foreach (var deinflection in deinflections)
                {
                    foreach (var entry in _dict.GetEntries(deinflection.Word))
                    {
                        if (DeinflectionIsValid(deinflection, entry))
                        {
                            results.Add(new SearchResult(entry, deinflection.Reason, matchLength));
                        }
                    }
                }

                searchString = searchString.Substring(0, searchString.Length - 1);
            } while(searchString.Length > 0);

            var resultFrequencies = results.ToDictionary(
                result => result,
                result => result.DictionaryEntry.SpellingElements.Max(element => GetFrequency(element.Spelling)));

            results.Sort((result1, result2) =>
            {
                var lengthCompare = result1.MatchLength.CompareTo(result2.MatchLength);

                if (lengthCompare != 0)
                {
                    // longest match first
                    return -lengthCompare;
                }
                else
                {
                    // most frequent result if equal match lengths
                    return resultFrequencies[result1].CompareTo(resultFrequencies[result2]);
                }
            });

            return results;
        }

        public Task<List<SearchResult>> GetWordsAsync(string searchString)
        {
            return Task.Run(() => GetWords(searchString));
        }

        private bool DeinflectionIsValid(Deinflector.DeinflectionResult deinflection, JdicEntry dicEntry)
        {
            if (deinflection.Type == 0xff) // no deinflection
            {
                return true;
            }

            foreach (var sense in dicEntry.SenseElements)
            {
                foreach (var pos in sense.PartsOfSpeech)
                {
                    if ((deinflection.Type & 1) != 0 && pos == "v1")
                    {
                        return true;
                    }

                    if ((deinflection.Type & 4) != 0 && pos == "adj-i")
                    {
                        return true;
                    }

                    if ((deinflection.Type & 66) != 0)
                    {
                        if (pos == "v5k-s" || pos == "v5u-s")
                        {
                            if ((deinflection.Type & 64) != 0)
                            {
                                return true;
                            }
                        }
                        else if (pos.StartsWith("v5"))
                        {
                            if ((deinflection.Type & 2) != 0)
                            {
                                return true;
                            }
                        }
                    }

                    if ((deinflection.Type & 8) != 0 && pos == "vk")
                    {
                        return true;
                    }

                    if ((deinflection.Type & 16) != 0 && pos.StartsWith("vs-"))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private int GetFrequency(string word)
        {
            int frequency;
            if (!_frequencies.TryGetValue(word, out frequency))
            {
                frequency = int.MaxValue;
            }

            return frequency;
        }
    }
}
