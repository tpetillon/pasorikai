﻿using System.Collections.Generic;
using System.Linq;

namespace Rikai
{
    public class Deinflector
    {
        public struct Rule
        {
            public readonly string From;
            public readonly string To;
            public readonly int Type;
            public readonly int Reason;

            public Rule(string from, string to, int type, int reason)
            {
                From = from;
                To = to;
                Type = type;
                Reason = reason;
            }
        }

        public struct RuleGroup
        {
            public readonly Rule[] Rules;
            public readonly int FromLength;

            public RuleGroup(Rule[] rules, int fromLength)
            {
                Rules = rules;
                FromLength = fromLength;
            }
        }

        public struct DeinflectionResult
        {
            public readonly string Word;
            public int Type;
            public readonly string Reason;

            public DeinflectionResult(string word, int type, string reason)
            {
                Word = word;
                Type = type;
                Reason = reason;
            }
        }

        private struct MutableDeinflectionResult
        {
            public string Word;
            public int Type;
            public string Reason;

            public DeinflectionResult ToImmutable()
            {
                return new DeinflectionResult(Word, Type, Reason);
            }
        }

        private List<string> _reasons;
        private List<RuleGroup> _ruleGroups;
        
        public Deinflector(List<string> reasons, List<RuleGroup> ruleGroups)
        {
            _reasons = reasons;
            _ruleGroups = ruleGroups;
        }

        public List<DeinflectionResult> Deinflect(string word)
        {
            var results = new List<MutableDeinflectionResult>();
            var have = new Dictionary<string, int>();

            results.Add(new MutableDeinflectionResult() { Word = word, Type = 0xff, Reason = "" });

            var i = 0;
            do
            {
                var w = results[i].Word;
                var wordLength = w.Length;
                var type = results[i].Type;

                foreach (var ruleGroup in _ruleGroups)
                {
                    if (ruleGroup.FromLength <= wordLength)
                    {
                        var end = w.Substring(w.Length - ruleGroup.FromLength, ruleGroup.FromLength);

                        foreach (var rule in ruleGroup.Rules)
                        {
                            if (((type & rule.Type) != 0) && (end == rule.From))
                            {
                                var newWord = w.Substring(0, w.Length - rule.From.Length) + rule.To;

                                if (newWord.Length <= 1)
                                {
                                    continue;
                                }

                                MutableDeinflectionResult o;

                                if (have.ContainsKey(newWord))
                                {
                                    o = results[have[newWord]];
                                    o.Type |= (rule.Type >> 8);
                                }

                                have[newWord] = results.Count;

                                if (results[i].Reason.Length > 0)
                                {
                                    o.Reason = _reasons[rule.Reason] + " > " + results[i].Reason;
                                }
                                else
                                {
                                    o.Reason = _reasons[rule.Reason];
                                }

                                o.Type = rule.Type >> 8;
                                o.Word = newWord;

                                results.Add(o);
                            }
                        }
                    }
                }

                i++;
            } while (i < results.Count);

            return results.Select(r => r.ToImmutable()).ToList();
        }
    }
}
