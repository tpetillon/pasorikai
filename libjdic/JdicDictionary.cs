﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;

namespace Jdic
{
    public class JdicDictionary
    {
        private readonly Dictionary<string, LinkedList<JdicEntry>> _entries;
        private int _maxEntryLength;

        public int EntryCount { get { return _entries.Count; } }

        public int MaxEntryLength { get { return _maxEntryLength; } }

        public JdicDictionary()
        {
            _entries = new Dictionary<string, LinkedList<JdicEntry>>();
            _maxEntryLength = 0;
        }

        public void AddEntries(IEnumerable<JdicEntry> entries)
        {
            foreach (var entry in entries)
            {
                foreach (var reading in entry.ReadingElements)
                {
                    AddEntry(reading.KanaSpelling, entry);
                }
                foreach (var spelling in entry.SpellingElements)
                {
                    AddEntry(spelling.Spelling, entry);
                }
            }
        }

        private void AddEntry(string key, JdicEntry entry)
        {
            var normalizedKey = KanaConverter.ToHiragana(key);

            GetOrCreateList(normalizedKey).AddLast(entry);

            if (normalizedKey.Length > _maxEntryLength)
            {
                _maxEntryLength = normalizedKey.Length;
            }
        }

        public IEnumerable<JdicEntry> GetEntries(string word)
        {
            var normalizedWord = KanaConverter.ToHiragana(word);

            LinkedList<JdicEntry> list = null;
            _entries.TryGetValue(normalizedWord, out list);

            if (list == null)
            {
                return Enumerable.Empty<JdicEntry>();
            }

            return list.Distinct();
        }

        private LinkedList<JdicEntry> GetOrCreateList(string reading)
        {
            LinkedList<JdicEntry> list = null;

            _entries.TryGetValue(reading, out list);

            if (list == null)
            {
                list = new LinkedList<JdicEntry>();
                _entries.Add(reading, list);
            }

            return list;
        }
    }
}
