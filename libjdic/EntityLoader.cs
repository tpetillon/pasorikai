﻿using System.IO;
namespace Jdic
{
    public class EntityLoader
    {
        public void LoadEntities(TextReader input, EntityCollection entities)
        {
            var splitChars = new char[] { '\t' };

            string line;
            while ((line = input.ReadLine()) != null)
            {
                var splits = line.Split(splitChars);

                entities.AddEntity(splits[0], splits[1]);
            }
        }

    }
}
