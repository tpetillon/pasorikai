﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace Jdic
{
    public class XmlKanjiLoader
    {
        public IEnumerable<CharacterElement> LoadCharacters(TextReader input)
        {
            var characters = new List<CharacterElement>();

            var settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.None;
            settings.ValidationFlags = XmlSchemaValidationFlags.None;
            using (var reader = XmlReader.Create(input, settings))
            {
                CharacterElement.Builder character = null;
                ReadingMeaningElement.Builder readingMeaning = null;

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (reader.Name)
                            {
                                case "character":
                                    character = CharacterElement.NewBuilder();
                                    break;
                                case "literal":
                                    if (ReadNextNode(reader))
                                    {
                                        character.SetLiteral(reader.Value);
                                    }
                                    break;
                                case "rmgroup":
                                    readingMeaning = ReadingMeaningElement.NewBuilder();
                                    break;
                                case "reading":
                                    var typeCode = reader.GetAttribute("r_type");
                                    var type = ReadingTypeFromCode(typeCode);
                                    if (ReadNextNode(reader))
                                    {
                                        readingMeaning.AddReading(reader.Value, type);
                                    }
                                    break;
                                case "meaning":
                                    var langCode = reader.GetAttribute("m_lang");
                                    var lang = langCode != null ? Languages.FromIso639_1(langCode) : Language.English;
                                    if (ReadNextNode(reader))
                                    {
                                        readingMeaning.AddMeaning(reader.Value, lang);
                                    }
                                    break;
                                case "nanori":
                                    if (ReadNextNode(reader))
                                    {
                                        character.AddNanori(reader.Value);
                                    }
                                    break;
                            }
                            break;
                        case XmlNodeType.EndElement:
                            switch (reader.Name)
                            {
                                case "character":
                                    characters.Add(character.Build());
                                    character = null;
                                    break;
                                case "rmgroup":
                                    character.AddReadingMeaning(readingMeaning.Build());
                                    readingMeaning = null;
                                    break;
                            }
                            break;
                    }
                }

                return characters;
            }
        }

        private ReadingMeaningElement.ReadingType ReadingTypeFromCode(string readingTypeCode)
        {
            switch(readingTypeCode)
            {
                case "ja_on":
                    return ReadingMeaningElement.ReadingType.JapaneseOn;
                case "ja_kun":
                    return ReadingMeaningElement.ReadingType.JapaneseKun;
                case "korean_r":
                    return ReadingMeaningElement.ReadingType.KoreanRoman;
                case "korean_h":
                    return ReadingMeaningElement.ReadingType.KoreanHangul;
                case "pinyin":
                    return ReadingMeaningElement.ReadingType.Pinyin;
                default:
                    return ReadingMeaningElement.ReadingType.Unknown;
            }
        }

        private bool ReadNextNode(XmlReader reader)
        {
            reader.Read();
            if (reader.HasValue && reader.NodeType == XmlNodeType.Text)
            {
                return true;
            }

            return false;
        }
    }
}
