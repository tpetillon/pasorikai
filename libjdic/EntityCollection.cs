﻿using System.Collections.Generic;

namespace Jdic
{
    public class EntityCollection
    {
        private readonly Dictionary<string, string> _entitiesToTexts;
        private readonly Dictionary<string, string> _textsToEntities;

        public EntityCollection()
        {
            _entitiesToTexts = new Dictionary<string, string>();
            _textsToEntities = new Dictionary<string, string>();
        }

        public void AddEntity(string entity, string text)
        {
            _entitiesToTexts.Add(entity, text);
            _textsToEntities.Add(text, entity);
        }

        public string GetEntityText(string entity)
        {
            return _entitiesToTexts[entity];
        }

        public string GetEntity(string text)
        {
            return _textsToEntities[text];
        }
    }
}
