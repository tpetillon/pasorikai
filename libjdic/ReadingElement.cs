﻿using System.Collections.Generic;

namespace Jdic
{
    public class ReadingElement
    {
        private readonly string _kanaSpelling;
        private readonly bool _trueReading;
        private readonly IList<SpellingElement> _restrictions;
        private readonly IList<string> _info;

        public string KanaSpelling { get { return _kanaSpelling; } }
        public bool TrueReading { get { return _trueReading; } }
        public IList<SpellingElement> Restrictions { get { return _restrictions; } }
        public IList<string> Info { get { return _info; } }

        private ReadingElement(string kanaSpelling, bool trueReading,
            IList<SpellingElement> restrictions, IList<string> info)
        {
            _kanaSpelling = kanaSpelling;
            _trueReading = trueReading;
            _restrictions = restrictions;
            _info = info;
        }

        public static Builder NewBuilder()
        {
            return new Builder();
        }

        public class Builder
        {
            private string _kanaSpelling;
            private bool _trueReading;
            private readonly List<SpellingElement> _restrictions;
            private readonly List<string> _info;

            public Builder()
            {
                _trueReading = true;
                _restrictions = new List<SpellingElement>();
                _info = new List<string>();
            }

            public Builder AddKanaSpelling(string kanaSpelling)
            {
                _kanaSpelling = kanaSpelling;
                return this;
            }

            public Builder IsTrueReading(bool trueReading)
            {
                _trueReading = trueReading;
                return this;
            }

            public Builder AddRestriction(SpellingElement spellingElement)
            {
                _restrictions.Add(spellingElement);
                return this;
            }

            public Builder AddInfo(string info)
            {
                _info.Add(info);
                return this;
            }

            public ReadingElement Build()
            {
                return new ReadingElement(
                    _kanaSpelling,
                    _trueReading,
                    _restrictions.AsReadOnly(),
                    _info.AsReadOnly());
            }
        }
    }
}
