﻿using System.Collections.Generic;

namespace Jdic
{
    public class SpellingElement
    {
        private readonly string _spelling;
        private readonly IList<string> _info;

        public string Spelling { get { return _spelling; } }
        public IList<string> Info { get { return _info; } }

        private SpellingElement(string spelling, IList<string> info)
        {
            _spelling = spelling;
            _info = info;
        }

        public static Builder NewBuilder()
        {
            return new Builder();
        }

        public class Builder
        {
            private string _spelling;
            private readonly List<string> _info;

            public Builder()
            {
                _info = new List<string>();
            }

            public Builder AddSpelling(string spelling)
            {
                _spelling = spelling;
                return this;
            }

            public Builder AddInfo(string info)
            {
                _info.Add(info);
                return this;
            }

            public SpellingElement Build()
            {
                return new SpellingElement(_spelling, _info.AsReadOnly());
            }
        }
    }
}
