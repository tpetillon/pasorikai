﻿namespace Jdic
{
    public static class Languages
    {
        public static Language FromIso639_2B(string langCode)
        {
            switch (langCode)
            {
                case "eng":
                    return Language.English;
                case "ger":
                    return Language.German;
                case "fre":
                    return Language.French;
                case "spa":
                    return Language.Spanish;
                case "rus":
                    return Language.Russian;
                case "dut":
                    return Language.Dutch;
                case "swe":
                    return Language.Swedish;
                case "hun":
                    return Language.Hungarian;
                case "slv":
                    return Language.Slovene;
                case "por":
                    return Language.Portuguese;
                default:
                    return Language.Unknown;
            }
        }

        public static Language FromIso639_1(string langCode)
        {
            switch (langCode)
            {
                case "en":
                    return Language.English;
                case "de":
                    return Language.German;
                case "fr":
                    return Language.French;
                case "es":
                    return Language.Spanish;
                case "ru":
                    return Language.Russian;
                case "nl":
                    return Language.Dutch;
                case "sv":
                    return Language.Swedish;
                case "hu":
                    return Language.Hungarian;
                case "sl":
                    return Language.Slovene;
                case "pt":
                    return Language.Portuguese;
                default:
                    return Language.Unknown;
            }
        }
    }
}
