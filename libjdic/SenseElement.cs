﻿using System.Collections.Generic;

namespace Jdic
{
    public class SenseElement
    {
        public struct Gloss
        {
            public readonly string Text;
            public readonly Language Language;

            public Gloss(string text, Language language)
            {
                Text = text;
                Language = language;
            }
        }

        private readonly IList<string> _partsOfSpeech;
        private readonly IList<string> _fields;
        private readonly IList<string> _miscInfo;
        private readonly IList<string> _dialects;
        private readonly IList<Gloss> _glosses;

        public IList<string> PartsOfSpeech { get { return _partsOfSpeech; } }
        public IList<string> Fields { get { return _fields; } }
        public IList<string> MiscInfo { get { return _miscInfo; } }
        public IList<string> Dialects { get { return _dialects; } }
        public IList<Gloss> Glosses { get { return _glosses; } }

        private SenseElement(IList<string> partsOfSpeech, IList<string> fields,
            IList<string> miscInfo, IList<string> dialects, IList<Gloss> glosses)
        {
            _partsOfSpeech = partsOfSpeech;
            _fields = fields;
            _miscInfo = miscInfo;
            _dialects = dialects;
            _glosses = glosses;
        }

        public static Builder NewBuilder()
        {
            return new Builder();
        }

        public class Builder
        {
            private readonly List<string> _partsOfSpeech;
            private readonly List<string> _fields;
            private readonly List<string> _miscInfo;
            private readonly List<string> _dialects;
            private readonly List<Gloss> _glosses;

            public Builder()
            {
                _partsOfSpeech = new List<string>();
                _fields = new List<string>();
                _miscInfo = new List<string>();
                _dialects = new List<string>();
                _glosses = new List<Gloss>();
            }

            public Builder AddPartOfSpeech(string partOfSpeech)
            {
                _partsOfSpeech.Add(partOfSpeech);
                return this;
            }

            public Builder AddField(string field)
            {
                _fields.Add(field);
                return this;
            }

            public Builder AddMiscInfo(string miscInfo)
            {
                _miscInfo.Add(miscInfo);
                return this;
            }

            public Builder AddDialect(string dialects)
            {
                _dialects.Add(dialects);
                return this;
            }

            public Builder AddGloss(string gloss, Language language)
            {
                _glosses.Add(new Gloss(gloss, language));
                return this;
            }

            public SenseElement Build()
            {
                return new SenseElement(
                    _partsOfSpeech.AsReadOnly(),
                    _fields.AsReadOnly(),
                    _miscInfo.AsReadOnly(),
                    _dialects.AsReadOnly(),
                    _glosses.AsReadOnly());
            }
        }
    }
}
