﻿using System.Collections.Generic;
using System.Text;

namespace Jdic
{
    public static class KanaConverter
    {
        public struct ConversionResult
        {
            public readonly string Text;
            public readonly int[] LengthMapping;

            public ConversionResult(string text, int[] lengthMapping)
            {
                Text = text;
                LengthMapping = lengthMapping;
            }
        }

        private static readonly char[] _ch = new char[]
        {
            '\u3092', '\u3041', '\u3043', '\u3045', '\u3047', '\u3049', '\u3083',
            '\u3085', '\u3087', '\u3063', '\u30FC', '\u3042', '\u3044', '\u3046',
            '\u3048', '\u304A', '\u304B', '\u304D', '\u304F', '\u3051', '\u3053',
            '\u3055', '\u3057', '\u3059', '\u305B', '\u305D', '\u305F', '\u3061',
            '\u3064', '\u3066', '\u3068', '\u306A', '\u306B', '\u306C', '\u306D',
            '\u306E', '\u306F', '\u3072', '\u3075', '\u3078', '\u307B', '\u307E',
            '\u307F', '\u3080', '\u3081', '\u3082', '\u3084', '\u3086', '\u3088',
            '\u3089', '\u308A', '\u308B', '\u308C', '\u308D', '\u308F', '\u3093'
        };

        private static readonly char[] _cv = new char[]
        {
            '\u30F4', '\uFF74', '\uFF75', '\u304C', '\u304E', '\u3050', '\u3052',
            '\u3054', '\u3056', '\u3058', '\u305A', '\u305C', '\u305E', '\u3060',
            '\u3062', '\u3065', '\u3067', '\u3069', '\uFF85', '\uFF86', '\uFF87',
            '\uFF88', '\uFF89', '\u3070', '\u3073', '\u3076', '\u3079', '\u307C'
        };

        private static readonly char[] _cs = new char[]
        {
            '\u3071', '\u3074', '\u3077', '\u307A', '\u307D'
        };

        public static string ToHiragana(string text)
        {
            return ConvertToHiragana(text, false).Text;
        }

        public static ConversionResult ToHiraganaWithLengthMapping(string text)
        {
            return ConvertToHiragana(text, true);
        }

        private static ConversionResult ConvertToHiragana(string text, bool computeLengthMapping)
        {
            var lengthMapping = computeLengthMapping ? new List<int>() { 0 } : null;
            var conv = new StringBuilder();
            char prev = '\0';

            for (var i = 0 ; i < text.Length ; i++)
            {
                var c = text[i];
                var originalChar = c;

                if (c < 0x3000)
                {
                    break;
                }

                // full-width katakana to hiragana
                if ((c >= 0x30A1) && (c <= 0x30F3))
                {
                    c = (char) (c - 0x60);
                }
                // half-width katakana to hiragana
                else if ((c >= 0xFF66) && (c <= 0xFF9D))
                {
                    c = _ch[c - 0xFF66];
                }
                // voiced (used in half-width katakana) to hiragana
                else if (c == 0xFF9E)
                {
                    if ((prev >= 0xFF73) && (prev <= 0xFF8E))
                    {
                        conv.Remove(conv.Length - 1, 1);
                        c = _cv[prev - 0xFF73];
                    }
                }
                // semi-voiced (used in half-width katakana) to hiragana
                else if (c == 0xFF9F)
                {
                    if ((prev >= 0xFF8A) && (prev <= 0xFF8E))
                    {
                        conv.Remove(conv.Length - 1, 1);
                        c = _cs[prev - 0xFF8A];
                    }
                }
                // ignore ~
                else if (c == 0xFF5E)
                {
                    prev = '\0';
                    continue;
                }

                conv.Append(c);

                // need to keep length mapping because of the half-width semi/voiced conversion
                if (computeLengthMapping)
                {
                    while (lengthMapping.Count < conv.Length + 1)
                    {
                        lengthMapping.Add(0);
                    }
                    lengthMapping[conv.Length] = i + 1;
                }

                prev = originalChar;
            }

            return new ConversionResult(
                conv.ToString(),
                computeLengthMapping ? lengthMapping.ToArray() : null);
        }
    }
}
