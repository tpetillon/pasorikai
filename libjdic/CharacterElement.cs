﻿using System.Collections.Generic;

namespace Jdic
{
    public class CharacterElement
    {
        private readonly string _literal;
        private readonly IList<ReadingMeaningElement> _readingMeanings;
        private readonly IList<string> _nanori;

        public string Literal { get { return _literal; } }
        public IList<ReadingMeaningElement> ReadingMeanings { get { return _readingMeanings; } }
        public IList<string> Nanori { get { return _nanori; } }

        public CharacterElement(string literal, IList<ReadingMeaningElement> readingMeanings,
            IList<string> nanori)
        {
            _literal = literal;
            _readingMeanings = readingMeanings;
            _nanori = nanori;
        }

        public static Builder NewBuilder()
        {
            return new Builder();
        }

        public class Builder
        {
            private string _literal;
            private readonly List<ReadingMeaningElement> _readingMeanings;
            private readonly List<string> _nanori;

            public Builder()
            {
                _readingMeanings = new List<ReadingMeaningElement>();
                _nanori = new List<string>();
            }

            public Builder SetLiteral(string literal)
            {
                _literal = literal;
                return this;
            }

            public Builder AddReadingMeaning(ReadingMeaningElement readingMeaning)
            {
                _readingMeanings.Add(readingMeaning);
                return this;
            }

            public Builder AddNanori(string nanori)
            {
                _nanori.Add(nanori);
                return this;
            }

            public CharacterElement Build()
            {
                return new CharacterElement(_literal, _readingMeanings, _nanori);
            }
        }
    }
}
