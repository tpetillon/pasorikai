﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace Jdic
{
    public class XmlDictionaryLoader
    {
        /// <note>
        /// Having to load a separate entity file and have the EntityCollection class is
        /// unpractical and cumbersome, but I haven't found a way to tell the XmlReader not
        /// to expand entities. No more luck when trying to get the entities from the DTD.
        /// </note>
        public IEnumerable<JdicEntry> LoadDictionary(TextReader input, EntityCollection entities)
        {
            var entries = new List<JdicEntry>();

            var settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.None;
            settings.ValidationFlags = XmlSchemaValidationFlags.None;
            using (var reader = XmlReader.Create(input, settings))
            {
                JdicEntry.Builder entry = null;
                SpellingElement.Builder spelling = null;
                ReadingElement.Builder reading = null;
                SenseElement.Builder sense = null;

                SenseElement previousSense = null;
                var partsOfSpeechSpecifiedForSense = false;
                var miscInfoSpecified = false;

                var currentSpellingElements = new Dictionary<string, SpellingElement>();

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (reader.Name)
                            {
                                case "entry":
                                    entry = JdicEntry.NewBuilder();
                                    break;
                                case "k_ele":
                                    spelling = SpellingElement.NewBuilder();
                                    break;
                                case "keb":
                                    if (ReadNextNode(reader))
                                    {
                                        spelling.AddSpelling(reader.Value);
                                    }
                                    break;
                                case "ke_inf":
                                    if (ReadNextNode(reader))
                                    {
                                        spelling.AddInfo(entities.GetEntity(reader.Value));
                                    }
                                    break;
                                case "r_ele":
                                    reading = ReadingElement.NewBuilder();
                                    break;
                                case "reb":
                                    if (ReadNextNode(reader))
                                    {
                                        reading.AddKanaSpelling(reader.Value);
                                    }
                                    break;
                                case "re_nokanji":
                                    reading.IsTrueReading(false);
                                    break;
                                case "re_restr":
                                    if (ReadNextNode(reader))
                                    {
                                        reading.AddRestriction(currentSpellingElements[reader.Value]);
                                    }
                                    break;
                                case "re_inf":
                                    if (ReadNextNode(reader))
                                    {
                                        reading.AddInfo(entities.GetEntity(reader.Value));
                                    }
                                    break;
                                case "sense":
                                    sense = SenseElement.NewBuilder();
                                    break;
                                case "pos":
                                    if (ReadNextNode(reader))
                                    {
                                        sense.AddPartOfSpeech(entities.GetEntity(reader.Value));
                                        partsOfSpeechSpecifiedForSense = true;
                                    }
                                    break;
                                case "field":
                                    if (ReadNextNode(reader))
                                    {
                                        sense.AddField(entities.GetEntity(reader.Value));
                                    }
                                    break;
                                case "misc":
                                    if (ReadNextNode(reader))
                                    {
                                        sense.AddMiscInfo(entities.GetEntity(reader.Value));
                                        miscInfoSpecified = true;
                                    }
                                    break;
                                case "dial":
                                    if (ReadNextNode(reader))
                                    {
                                        sense.AddDialect(entities.GetEntity(reader.Value));
                                    }
                                    break;
                                case "gloss":
                                    var langCode = reader.GetAttribute("xml:lang");
                                    var lang = langCode != null ? Languages.FromIso639_2B(langCode) : Language.English;
                                    if (ReadNextNode(reader))
                                    {
                                        sense.AddGloss(reader.Value, lang);
                                    }
                                    break;
                            }
                            break;
                        case XmlNodeType.EndElement:
                            switch (reader.Name)
                            {
                                case "entry":
                                    entries.Add(entry.Build());
                                    currentSpellingElements.Clear();
                                    previousSense = null;
                                    break;
                                case "k_ele":
                                    var spellingElement = spelling.Build();
                                    entry.AddSpellingElement(spellingElement);
                                    currentSpellingElements.Add(spellingElement.Spelling, spellingElement);
                                    break;
                                case "r_ele":
                                    entry.AddReadingElement(reading.Build());
                                    break;
                                case "sense":
                                    if (partsOfSpeechSpecifiedForSense == false && previousSense != null)
                                    {
                                        foreach (var partOfSpeech in previousSense.PartsOfSpeech)
                                        {
                                            sense.AddPartOfSpeech(partOfSpeech);
                                        }
                                    }
                                    if (miscInfoSpecified == false && previousSense != null)
                                    {
                                        foreach (var miscInfo in previousSense.MiscInfo)
                                        {
                                            sense.AddMiscInfo(miscInfo);
                                        }
                                    }
                                    var builtSense = sense.Build();
                                    entry.AddSenseElement(builtSense);
                                    previousSense = builtSense;
                                    partsOfSpeechSpecifiedForSense = false;
                                    miscInfoSpecified = false;
                                    break;
                            }
                            break;
                    }
                }
            }

            return entries;
        }

        private bool ReadNextNode(XmlReader reader)
        {
            reader.Read();
            if (reader.HasValue && reader.NodeType == XmlNodeType.Text)
            {
                return true;
            }

            return false;
        }
    }
}
