﻿namespace Jdic
{
    public enum Language
    {
        English,
        German,
        French,
        Spanish,
        Russian,
        Dutch,
        Swedish,
        Hungarian,
        Slovene,
        Portuguese,
        Unknown
    }
}
