﻿using System.Collections.Generic;
namespace Jdic
{
    public class KanjiDictionary
    {
        private readonly Dictionary<string, CharacterElement> _entries;

        public int EntryCount { get { return _entries.Count; } }

        public KanjiDictionary()
        {
            _entries = new Dictionary<string, CharacterElement>();
        }

        public void AddEntries(IEnumerable<CharacterElement> entries)
        {
            foreach (var entry in entries)
            {
                _entries.Add(entry.Literal, entry);
            }
        }

        public CharacterElement GetEntry(string literal)
        {
            CharacterElement entry = null;
            _entries.TryGetValue(literal, out entry);
            return entry;
        }
    }
}
