﻿using System.Collections.Generic;

namespace Jdic
{
    public class ReadingMeaningElement
    {
        public enum ReadingType
        {
            JapaneseOn,
            JapaneseKun,
            KoreanRoman,
            KoreanHangul,
            Pinyin,
            Unknown
        }

        public struct TypedReading
        {
            public readonly string Reading;
            public readonly ReadingType Type;

            public TypedReading(string reading, ReadingType type)
            {
                Reading = reading;
                Type = type;
            }
        }

        public struct MeaningAndLanguage
        {
            public readonly string Meaning;
            public readonly Language Language;

            public MeaningAndLanguage(string meaning, Language language)
            {
                Meaning = meaning;
                Language = language;
            }
        }

        private readonly IList<TypedReading> _readings;
        private readonly IList<MeaningAndLanguage> _meanings;

        public IList<TypedReading> Readings { get { return _readings; } }
        public IList<MeaningAndLanguage> Meanings { get { return _meanings; } }

        public ReadingMeaningElement(IList<TypedReading> readings, IList<MeaningAndLanguage> meanings)
        {
            _readings = readings;
            _meanings = meanings;
        }

        public static Builder NewBuilder()
        {
            return new Builder();
        }

        public class Builder
        {
            private readonly List<TypedReading> _readings;
            private readonly List<MeaningAndLanguage> _meanings;

            public Builder()
            {
                _readings = new List<TypedReading>();
                _meanings = new List<MeaningAndLanguage>();
            }

            public Builder AddReading(string reading, ReadingType type)
            {
                _readings.Add(new TypedReading(reading, type));
                return this;
            }

            public Builder AddMeaning(string meaning, Language language)
            {
                _meanings.Add(new MeaningAndLanguage(meaning, language));
                return this;
            }

            public ReadingMeaningElement Build()
            {
                return new ReadingMeaningElement(
                    _readings.AsReadOnly(),
                    _meanings.AsReadOnly());
            }
        }
    }
}
