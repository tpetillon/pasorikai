﻿using System.Collections.Generic;
using System.Linq;

namespace Jdic
{
    public class JdicEntry
    {
        private readonly IList<SpellingElement> _spellingElements;
        private readonly IList<ReadingElement> _readingElements;
        private readonly IList<SenseElement> _senseElements;

        public IList<SpellingElement> SpellingElements { get { return _spellingElements; } }
        public IList<ReadingElement> ReadingElements { get { return _readingElements; } }
        public IList<SenseElement> SenseElements { get { return _senseElements; } }

        private JdicEntry(IList<SpellingElement> spellingElements, IList<ReadingElement> readingElements,
            IList<SenseElement> senseElements)
        {
            _spellingElements = spellingElements;
            _readingElements = readingElements;
            _senseElements = senseElements;
        }

        public static Builder NewBuilder()
        {
            return new Builder();
        }

        public class Builder
        {
            private readonly List<SpellingElement> _spellingElements;
            private readonly List<ReadingElement> _readingElements;
            private readonly List<SenseElement> _senseElements;

            public bool HasSpellingElements { get { return _spellingElements.Count > 0; } }

            public Builder()
            {
                _spellingElements = new List<SpellingElement>();
                _readingElements = new List<ReadingElement>();
                _senseElements = new List<SenseElement>();
            }

            public Builder AddSpellingElement(SpellingElement spellingElement)
            {
                _spellingElements.Add(spellingElement);
                return this;
            }

            public Builder AddReadingElement(ReadingElement readingElement)
            {
                _readingElements.Add(readingElement);
                return this;
            }

            public Builder AddSenseElement(SenseElement senseElement)
            {
                _senseElements.Add(senseElement);
                return this;
            }

            public JdicEntry Build()
            {
                if (_spellingElements.Count == 0)
                {
                    // no spelling elements, the reading elements define the entry
                    _spellingElements.AddRange(_readingElements.Select(
                        el => SpellingElement.NewBuilder().AddSpelling(el.KanaSpelling).Build()));
                }

                return new JdicEntry(
                    _spellingElements.AsReadOnly(),
                    _readingElements.AsReadOnly(),
                    _senseElements.AsReadOnly());
            }
        }
    }
}
