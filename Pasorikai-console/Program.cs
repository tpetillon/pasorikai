﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Runtime.InteropServices;
using Jdic;
using Rikai;
using System.Collections.Generic;

namespace PasorikaiConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            SetConsoleOutputCP(932);

            var dict = new JdicDictionary();
            var entities = new EntityCollection();
            Dictionary<string, int> frequencies;

            using (var entityFileReader = new StreamReader("entities.dat"))
            {
                var entityLoader = new EntityLoader();
                entityLoader.LoadEntities(entityFileReader, entities);
            }

            using (var dictFileReader = new StreamReader("JMdict"))
            {
                var dictLoader = new XmlDictionaryLoader();
                var entries = dictLoader.LoadDictionary(dictFileReader, entities);
                dict.AddEntries(entries);
            }

            using (var frequencyFileReader = new StreamReader("freq.dat"))
            {
                var frequencyLoader = new FrequencyLoader();
                frequencies = frequencyLoader.LoadFrequencies(frequencyFileReader);
            }

            var deinflector = BuildDeinflector("deinflect.dat");

            var wordRetriever = new WordRetriever(dict, frequencies, deinflector);

            Console.WriteLine();
            Console.WriteLine(dict.EntryCount + " entries loaded");
            Console.WriteLine("Max entry length: " + dict.MaxEntryLength);
            Console.WriteLine();

            Console.WriteLine("Entries for さくら:");
            foreach (var entry in dict.GetEntries("さくら"))
            {
                Console.WriteLine(EntryToString(entry));
            }

            Console.WriteLine("Deinflecting and searching 眠りたくなかった:");
            var deinflectionResults = deinflector.Deinflect("眠りたくなかった");
            Console.WriteLine(DeinflectionResultsToString(deinflectionResults));
            Console.WriteLine();

            foreach (var deinflectionResult in deinflectionResults)
            {
                foreach (var entry in dict.GetEntries(deinflectionResult.Word))
                {
                    Console.WriteLine(EntryToString(entry, deinflectionResult.Reason));
                }
            }

            Console.WriteLine();
            Console.WriteLine("Searching みなかった:");
            foreach (var searchResult in wordRetriever.GetWords("みなかった"))
            {
                Console.WriteLine("Match with length " + searchResult.MatchLength + ":");
                Console.WriteLine(EntryToString(searchResult.DictionaryEntry, searchResult.Deinflection));
            }
        }

        private static Deinflector BuildDeinflector(string ruleFilePath)
        {
            using (var ruleFileReader = new StreamReader(ruleFilePath))
            {
                var ruleLoader = new DeinflectionRuleLoader();
                var reasonsAndRules = ruleLoader.LoadRules(ruleFileReader);

                Console.WriteLine("Loaded " + reasonsAndRules.Item1.Count +
                    " deinflect reasons and " + reasonsAndRules.Item2.SelectMany(ruleGroup => ruleGroup.Rules).Count() +
                    " deinflect rules in " + reasonsAndRules.Item2.Count + " groups");

                return new Deinflector(reasonsAndRules.Item1, reasonsAndRules.Item2);
            }
        }

        private static string EntryToString(JdicEntry entry, string deinflectionReason = null)
        {
            var s = new StringBuilder();
            
            s.Append(string.Join(", ", entry.SpellingElements.Select(k => k.Spelling)));

            if (!string.IsNullOrEmpty(deinflectionReason))
            {
                s.Append(" < ");
                s.Append(deinflectionReason);
            }

            s.AppendLine();
            s.Append(string.Join(", ", entry.ReadingElements.Select(r => r.KanaSpelling)));
            s.AppendLine();

            var i = 1;
            foreach (var sense in entry.SenseElements)
            {
                s.Append(i);
                s.Append(" (");
                s.Append(string.Join(", ", sense.PartsOfSpeech));
                s.Append("): ");
                s.Append(string.Join(", ", sense.Glosses.Where(gloss => gloss.Language == Language.English).Select(gloss => gloss.Text)));
                s.AppendLine();
                i++;
            }
            

            return s.ToString();
        }

        private static string DeinflectionResultsToString(List<Deinflector.DeinflectionResult> results)
        {
            var s = new StringBuilder();
            
            var i = 1;
            foreach (var result in results)
            {
                s.Append(i);
                s.Append(" ");
                s.Append(result.Word);
                s.Append(" (");
                s.Append(result.Reason);
                s.Append(")");
                s.AppendLine();
                i++;
            }

            return s.ToString();
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int SetConsoleOutputCP(int wCodePageID);

    }
}
