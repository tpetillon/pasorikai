# Pasorikai

[Homepage](http://www.thom.space/projects/pasorikai/)

A Japanese-to-English interactive dictionary for Windows. Based on a port of the [Rikaichan](http://www.polarcloud.com/rikaichan) Firefox extension. Uses [Jim Breen’s WWWJDIC](http://wwwjdic.com/) Japanese dictionary.

Features:
* Automatically grabs text for the clipboard,
* Can be controlled without application focus, using keyboard hotkeys,
* Conjugated form to root form conversion,
* Kanji to kana conversion,
* Kanji details display,
* Results sorted by usage frequency.

A Visual Studio 2013 project is provided in the repository.

Licence: [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)
